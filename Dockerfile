FROM archlinux

COPY pacman.conf /etc/pacman.conf

RUN pacman -Syu --noconfirm --needed 2048-py paru neofetch bpytop

RUN pacman -Scc --noconfirm # clear caches
